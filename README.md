## OAuth2 Experiment using Spring Boot 

This is just a simple service to identify the essential configuration required to secure a 
REST endpoint in Spring Boot as described in my [blog post](https://www.englishteeth.com/posts/2020-02-27-oauth-boot-experiment/).

Most examples of this I have found when searching seemed to always stand up a local authentication 
server or would include some proprietary library. The 
[guide on spring.io](https://spring.io/guides/tutorials/spring-boot-oauth2/#github-register-application) 
is really good, but still does more than is needed. 
The intention here is to keep the code as simple and as standards based as possible.

As such, the `pom.xml` brings in only core spring libraries.
```xml
  <dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-security</artifactId>
  </dependency>
  <dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-oauth2-client</artifactId>
  </dependency>
  <dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-web</artifactId>
  </dependency>
```
The template `application.yml` shows the configuration for [okta](https://www.okta.com/) and 
[GitHub](https://github.com/) as OAuth providers.
```yaml
spring:
  security:
    oauth2:
      client:
        registration:
          github:
            clientId: github-client-id
            clientSecret: github-client-secret
          okta:
            client-id: okta-client-id
            client-authentication-method: none
            authorization-grant-type: authorization_code
        provider:
          okta:
            authorization-uri: https://your-subdomain.okta.com/oauth2/v1/authorize
            token-uri: https://your-subdomain.okta.com/oauth2/v1/token
            user-info-uri: https://your-subdomain.okta.com/oauth2/v1/userinfo
            jwk-set-uri: https://your-subdomain.okta.com/oauth2/v1/keys
```

Obviously, to use GitHub’s OAuth 2.0 authentication system for login, you must first 
[Add a new GitHub app](https://github.com/settings/developers)

Select "New OAuth App" and then the "Register a new OAuth application" page is presented. 
Enter an app name and description. Then, enter your app’s home page, which should be 
`http://localhost:8080`, in this case and the Authorization callback URL as 
`http://localhost:8080/login/oauth2/code/github` and click Register Application.

The OAuth redirect URI is the path in the application that the end-user’s user-agent is redirected 
back to after they have authenticated with GitHub and have granted access to the application on the
Authorize application page.
          
Similarly to use Okta, you need to create an OIDC application on Okta. 
If you don’t have an Okta developer account, create one at developer.okta.com/signup.

The process to connect is described in more detail in my [blog post](https://www.englishteeth.com/posts/2020-02-27-oauth-boot-experiment/) 
and in the [Okta help pages](https://help.okta.com/en/prod/Content/Topics/Apps/Apps_App_Integration_Wizard.htm).
                       
After configuring the provider(s) and the service is started, the endpoint 
`http://localhost:8080/message` should present a login challenge and once authenticated, the user id 
provided by the authentication provider.

