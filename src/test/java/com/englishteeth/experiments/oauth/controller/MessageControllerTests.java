package com.englishteeth.experiments.oauth.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.security.Principal;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class MessageControllerTests {

  @Mock
  private Principal principal;

  @InjectMocks
  private MessageController messageController;

  @Test
  public void testMessageEndpointReturnsUser() {
    when(principal.getName()).thenReturn("test");

    String message = messageController.getMessage(principal);

    assertEquals("The message is for user: test", message);
  }


}
